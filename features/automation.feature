Feature: setting up an account and shopping on website
    Scenario:  setting up an account with valid data
        Given the user is on Automationpractice website
        When the user creates an account with valid data
        Then the user is logged into his/her account

    Scenario:  shopping on website and choose one product
        Given the user is logged in to the Automationpractice website
        When the user adds the product to the cart and continues until the order is confirmed
        Then the user has purchased the product
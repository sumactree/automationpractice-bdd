from behave import *
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
import time
import os
from random import randint
from faker import Faker



# TC 1 - Creating an account on http://automationpractice.com/index.php
# TC 1.1 - Setting up an account with correct data

# Data :
fake = Faker()
first_name = fake.first_name()
last_name = fake.last_name()
phone_number = fake.random_number(digits=10)
post_code = fake.postcode()
street_name = fake.street_name()
city = fake.city()
state = fake.state()
birth_year = str(fake.year())
day_number = str(fake.random_digit_not_null())
month_number = str(fake.random_digit_not_null())
password = fake.random_number(digits=10)


@given ('the user is on Automationpractice website')
def step_start_page (context):
    context.driver.get("http://automationpractice.com/index.php")
    context.driver.maximize_window()
    context.driver.implicitly_wait(20)

@when('the user creates an account with valid data')
def step_create_account(context):
    # 1 Click on button -> ,,Sign In" 
    context.driver.find_element_by_xpath('//*[@id="header"]/div[2]/div/div/nav/div[1]/a').click();

    # 2 Type in the field pod Create an account email ->leruv@19jo-mail.com
    mail = str(randint(0,9999)) + '@19jo-mail.com'
    context.driver.find_element_by_xpath('//*[@id="email_create"]').send_keys(mail);

    # 3 Click on button -> ,,Create an account" 
    context.driver.find_element_by_xpath('//*[@id="SubmitCreate"]/span').click();

    # 4 Choose option in Title -> Mrs
    context.driver.find_element_by_xpath('//*[@id="id_gender2"]').click();

    # 5 Type in the field First name  
    context.driver.find_element_by_xpath('//*[@id="customer_firstname"]').send_keys(first_name);

    # 6 Type in the field Last name  
    context.driver.find_element_by_xpath('//*[@id="customer_lastname"]').send_keys(last_name);

    # 7 Type in the field Password -> 10 digits
    context.driver.find_element_by_xpath('//*[@id="passwd"]').send_keys(password);

    # 8 Select the date options under the name Date of birth ->      
    # a) Day  
    select = Select(context.driver.find_element_by_xpath('//*[@id="days"]'))
    select.select_by_value(day_number);

    # b) Month
    select = Select(context.driver.find_element_by_xpath('//*[@id="months"]'))
    select.select_by_value(month_number);

    # c) Birth year 
    select = Select(context.driver.find_element_by_xpath('//*[@id="years"]'))
    select.select_by_value(birth_year);

    # 9 Type in the field Address
    context.driver.find_element_by_xpath('//*[@id="address1"]').send_keys(street_name);

    # 10 Type in the field City
    context.driver.find_element_by_xpath('//*[@id="city"]').send_keys(city);

    # 11 Choose option in State -> 
    select = Select(context.driver.find_element_by_xpath('//*[@id="id_state"]'))
    select.select_by_visible_text('California');

    # 12 Type in the field Zip/Postal Code
    context.driver.find_element_by_xpath('//*[@id="postcode"]').send_keys(post_code);

    # 13 Choose option in Country (it is only one option) -> United States
    select = Select(context.driver.find_element_by_xpath('//*[@id="id_country"]'))
    select.select_by_visible_text('United States');

    # 14 Type in the field Mobile phone 
    context.driver.find_element_by_xpath('//*[@id="phone_mobile"]').send_keys(phone_number);

    # 15 Type in the field Assign an address alias for future reference. -> leruv@19jo-mail.com 
    context.driver.find_element_by_xpath('//*[@id="alias"]').clear()
    context.driver.find_element_by_xpath('//*[@id="alias"]').send_keys(mail);
    context.driver.save_screenshot("screenshots/TC 1 - 1.1.png")

    # 16 Click on button -> ,,Register"  
    context.driver.find_element_by_xpath('//*[@id="submitAccount"]/span').click();
    time.sleep(5)

@then('the user is logged into his/her account')
def step_login(context):
    context.driver.save_screenshot("screenshots/TC 1 - 1.2.png")
    assert context.driver.find_element_by_tag_name('body')


# TC 3 - Making a purchase in the online store http://automationpractice.com/index.php
# TC 3.1 - Product purchase

@given ('the user is logged in to the Automationpractice website')
def step_start_page (context):
    context.driver.get("http://automationpractice.com/index.php")
    context.driver.maximize_window()
    context.driver.implicitly_wait(20)

@when('the user adds the product to the cart and continues until the order is confirmed')
def step_add_product(context):
    # 1
    context.driver.find_element_by_xpath('//*[@id="block_top_menu"]/ul/li[1]/a').click();
    # 2
    context.driver.find_element_by_xpath('//*[@id="center_column"]/ul/li[1]/div/div[2]/h5/a').click();
    # 3
    context.driver.find_element_by_xpath('//*[@id="quantity_wanted"]').clear()
    context.driver.find_element_by_xpath('//*[@id="quantity_wanted"]').send_keys('1');
    # 4
    select = Select(context.driver.find_element_by_xpath('//*[@id="group_1"]'))
    select.select_by_visible_text('S');
    # 5
    context.driver.find_element_by_xpath('//*[@id="color_13"]').click();
    # 6
    context.driver.find_element_by_xpath('//*[@id="add_to_cart"]/button/span').click();
    # 7
    context.driver.find_element_by_xpath('//*[@id="layer_cart"]/div[1]/div[2]/div[4]/a/span').click();
    # 8
    context.driver.find_element_by_xpath('//*[@id="center_column"]/p[2]/a[1]').click();
    # 9
    context.driver.find_element_by_xpath('//*[@id="center_column"]/form/p/button').click();
    # 10
    context.driver.find_element_by_xpath('//*[@id="cgv"]').click();
    # 11
    context.driver.find_element_by_xpath('//*[@id="form"]/p/button').click();
    # 12
    context.driver.find_element_by_xpath('//*[@id="HOOK_PAYMENT"]/div[1]/div/p/a').click();
    # 13
    context.driver.find_element_by_xpath('//*[@id="cart_navigation"]/button').click();
    # 14
    context.driver.find_element_by_xpath('//*[@id="header"]/div[2]/div/div/nav/div[2]/a').click();


@then('the user has purchased the product')
def step_login_in(context):
    context.driver.save_screenshot("screenshots/TC 3 - 3.1.png")
    assert context.driver.find_element_by_tag_name('body')

